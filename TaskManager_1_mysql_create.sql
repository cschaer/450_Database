CREATE TABLE `Employee` (
	`Employee_ID` INT NOT NULL AUTO_INCREMENT,
	`First_Name` varchar(30),
	`Last_Name` varchar(50) NOT NULL,
	`Admin` BOOLEAN NOT NULL,
	PRIMARY KEY (`Employee_ID`)
);

CREATE TABLE `Task` (
	`Task_ID` INT NOT NULL AUTO_INCREMENT,
	`Title` varchar(30) NOT NULL,
	`Description` varchar(400),
	`Status_ID` INT NOT NULL,
	`Creation_TS` TIMESTAMP NOT NULL,
	`Update_TS` TIMESTAMP NOT NULL,
	`Created_by` INT NOT NULL,
	`Updated_by` INT NOT NULL,
	PRIMARY KEY (`Task_ID`)
);

CREATE TABLE `Employee_Task` (
	`Employee_ID` INT NOT NULL,
	`Task_ID` INT NOT NULL,
	PRIMARY KEY (`Employee_ID`,`Task_ID`)
);

CREATE TABLE `Status` (
	`Status_ID` INT NOT NULL AUTO_INCREMENT,
	`Description` varchar(50),
	PRIMARY KEY (`Status_ID`)
);

CREATE TABLE `Task_Event` (
	`Event_ID` INT NOT NULL AUTO_INCREMENT,
	`Task_ID` INT NOT NULL,
	`Event_TS` TIMESTAMP NOT NULL,
	`Employee_ID` INT NOT NULL,
	PRIMARY KEY (`Event_ID`,`Task_ID`)
);

CREATE TABLE `Event` (
	`Event_ID` INT NOT NULL AUTO_INCREMENT,
	`Title` varchar(15) NOT NULL,
	`Description` varchar(50) NOT NULL,
	PRIMARY KEY (`Event_ID`)
);

ALTER TABLE `Task` ADD CONSTRAINT `Task_fk0` FOREIGN KEY (`Status_ID`) REFERENCES `Status`(`Status_ID`);

ALTER TABLE `Task` ADD CONSTRAINT `Task_fk1` FOREIGN KEY (`Created_by`) REFERENCES `Employee`(`Employee_ID`);

ALTER TABLE `Task` ADD CONSTRAINT `Task_fk2` FOREIGN KEY (`Updated_by`) REFERENCES `Employee`(`Employee_ID`);

ALTER TABLE `Employee_Task` ADD CONSTRAINT `Employee_Task_fk0` FOREIGN KEY (`Employee_ID`) REFERENCES `Employee`(`Employee_ID`);

ALTER TABLE `Employee_Task` ADD CONSTRAINT `Employee_Task_fk1` FOREIGN KEY (`Task_ID`) REFERENCES `Task`(`Task_ID`);

ALTER TABLE `Task_Event` ADD CONSTRAINT `Task_Event_fk0` FOREIGN KEY (`Event_ID`) REFERENCES `Event`(`Event_ID`);

ALTER TABLE `Task_Event` ADD CONSTRAINT `Task_Event_fk1` FOREIGN KEY (`Task_ID`) REFERENCES `Task`(`Task_ID`);

ALTER TABLE `Task_Event` ADD CONSTRAINT `Task_Event_fk2` FOREIGN KEY (`Employee_ID`) REFERENCES `Employee`(`Employee_ID`);

